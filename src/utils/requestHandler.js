import axios from 'axios';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

export function sendRequest(method, url, data){
    const headers={
        'content-Type': 'application/json; charset=utf-8',
        'x-access-token': cookies.get('auth').token
    };
    return axios({ method: method, url: url, headers: headers, data: data })
        .then((res) => {
            return res
        })
        .catch((err) => {
            return err
        }
    );
};

export function loginRequest(options){
    const headers={
        'content-Type': 'application/json; charset=utf-8'
    };
    const data = options;
    return axios({ method: 'POST', url: '/api/auth', headers: headers, data: data })
        .then((res) => {
            return res
        })
        .catch((err) => {
            return err
        }
    );
}

export function registerRequest(options){
    const headers={
        'content-Type': 'application/json; charset=utf-8'
    };
    const data = {
        username: options.username,
        password: options.password,
        mail: options.email
    };
    return axios({ method: 'POST', url: '/api/users', headers: headers, data: data })
        .then((res) => {
            return res
        })
        .catch((err) => {
            return err
        }
    );
}