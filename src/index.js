import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import  createHistory  from 'history/createBrowserHistory';

import configureStore from './rootReducer/configureStore';
import App from './containers/App';
import './style/style.css';

const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);
const MOUNT_NODE = document.getElementById('root');
const element = (
  <Provider store={store}>
        <ConnectedRouter history={history}>
          <App />
        </ConnectedRouter>
    </Provider>
);

const render = () => {
  ReactDOM.render(element,MOUNT_NODE);
};

render();
