import React from 'react';
import ReactDOM from 'react-dom';

import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import { withRouter } from "react-router";
import { connect } from 'react-redux';
import { compose } from 'redux';

import reducer from './profile.reducer';
import { updateUser, deleteUser, getUser } from './profile.actions';
import { makeSelectUser } from './profile.selector';

import {  Form, Input, Row, Col, Button} from 'antd';
import './profile.styles.scss'
import Cookies from 'universal-cookie';

const cookies = new Cookies();

const FormItem = Form.Item;

export class Login extends React.Component {
    componentWillMount(){
        this.props.getUser(cookies.get("auth").id);
    }

    componentDidUpdate(prevProps){
        if(this.props.user !== prevProps.user){
            this.setState({user : this.props.user})
        }
    }

    state = {
        auth: "",
        confirmDirty: false,
        user:{}
    }

    update= (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            if(values.password === null){
                delete values.password;
            }
            this.props.updateUser(this.state.user._id, values);
          }
        });
      }

    delete = (id) => {
        this.props.deleteUser(id);
        cookies.remove("auth");
        this.props.history.push('/login')
    }


    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
          };

          const tailFormItemLayout = {
            wrapperCol: {
              xs: {
                span: 24,
                offset: 0,
              },
              sm: {
                span: 24,
                offset: 0,
              },
            },
          };

        return (
            <Row className="profile">
                <Col xs={24} sm={24} md={24} lg={24} xl={24}>
                    <div className="profile-content">
                    <div className="profile-content__label">Personal data</div>
                    <Form className={"profile-content__form"} onSubmit={this.update}>
                        <FormItem
                            {...formItemLayout}
                            label="E-mail"
                            >
                            {getFieldDecorator('email', {
                                rules: [{
                                type: 'email', message: 'The input is not valid E-mail!',
                                }],
                                initialValue:this.state.user.mail
                            })(
                                <Input  />
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="Password"
                            >
                            {getFieldDecorator('password', {
                                initialValue:null
                            })(
                                <Input  />
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label={(
                                <span>
                                Username
                                </span>
                            )}
                            >
                            {getFieldDecorator('username', {
                                initialValue:this.state.user.username
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label={(
                                <span>
                                Name
                                </span>
                            )}
                            >
                            {getFieldDecorator('name', {
                                initialValue:this.state.user.name
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label={(
                                <span>
                                Surname
                                </span>
                            )}
                            >
                            {getFieldDecorator('surname', {
                                initialValue:this.state.user.surname
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label={(
                                <span>
                                City
                                </span>
                            )}
                            >
                            {getFieldDecorator('city', {
                                initialValue:this.state.user.city
                            })(
                                <Input  />
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label={(
                                <span>
                                Age
                                </span>
                            )}
                            >
                            {getFieldDecorator('age', {
                                initialValue:this.state.user.age
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit">Save</Button>
                            <Button type="primary" className="profile-content__delete" onClick={() => {this.delete(this.state.user._id)}}>Delete account</Button>
                        </FormItem>
                    </Form>
                    </div>
                </Col>
            </Row>
        )
    }
}

const withReducer = injectReducer({ key: 'user', reducer });

const mapStateToProps = createStructuredSelector({
    user: makeSelectUser(),
});

const mapDispatchToProps = { updateUser, deleteUser, getUser  };

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default withRouter(compose(
    withReducer,
    withConnect,
    Form.create(),
)(Login));