export const UPDATE_SUCCESS = 'UPDATE_SUCCESS';
export const UPDATE_FAILED = 'UPDATE_FAILED';
export const DELETE_SUCCESS = 'DELETE_SUCCESS';
export const DELETE_FAILED = 'DELETE_FAILED';
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const GET_USER_FAILED = 'GET_USER_FAILED';

import { sendRequest } from '../../utils/requestHandler';

export function updateUser(id, data) {
  return (dispatch) => {
    sendRequest('PATCH', '/api/users/' + id, data).then(resp => {
        dispatch({
            type: UPDATE_SUCCESS,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: UPDATE_FAILED
      });
    })
  }
}

export function deleteUser(id) {
  return (dispatch) => {
    sendRequest('DELETE', '/api/users/' + id, {}).then(resp => {
        dispatch({
            type: DELETE_SUCCESS,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: DELETE_FAILED
      });
    })
  }
}

export function getUser(id) {
  return (dispatch) => {
    sendRequest('GET', '/api/users/' + id, {}).then(resp => {
        dispatch({
            type: GET_USER_SUCCESS,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: GET_USER_FAILED
      });
    })
  }
}
