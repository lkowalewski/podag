import { fromJS } from 'immutable';

import {
  GET_USER_SUCCESS,
  GET_USER_FAILED,
} from './profile.actions';

const initialState = fromJS({
  user: {},
});

function profileReducer(state = initialState, action) {
  switch (action.type) {
    case GET_USER_SUCCESS: {
      return state.set('user', action.payLoad)
    }
    case GET_USER_FAILED: {
      return state;
    }
    default:
      return state;
  }
}

export default profileReducer;