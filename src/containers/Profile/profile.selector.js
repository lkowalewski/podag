import { createSelector } from 'reselect';

const selectUser= (state) => state.get('user');

const makeSelectUser = () => createSelector(
  selectUser,
  (userState) => userState.get('user')
);

export {
  selectUser,
  makeSelectUser
};