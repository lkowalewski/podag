import { fromJS } from 'immutable';
import Cookies from 'universal-cookie';

import {
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
} from '../Login/login.actions';

const cookies = new Cookies();

const initialState = fromJS({
  auth: {},
  alert:{}
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      cookies.set('auth', { token:action.payLoad.token, username:action.payLoad.username, id:action.payLoad.id });
      return state.set('auth', cookies.get('auth'))
    }
    case LOGIN_FAILED: {
      return state.set('auth', {})
    }
    case REGISTER_SUCCESS: {
      return state;
    }
    case REGISTER_FAILED: {
      return state;
    }
    default:
      return state;
  }
}

export default appReducer;