import React from 'react';
import ReactDOM from 'react-dom';

import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import { withRouter } from "react-router";
import { connect } from 'react-redux';
import { compose } from 'redux';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import reducer from './app.reducer';
import { login } from '../Login/login.actions';
import { makeSelectAuth } from './app.selector';

import { Button, Icon, Row, Col, Menu } from 'antd';
import './app.styles.scss'

import Login from '../Login';
import Home from '../Home';
import Profile from '../Profile';
import Place from '../Place';
import Recomended from '../Recomended';

import Cookies from 'universal-cookie';

const cookies = new Cookies();

const ProtectedRoute = ({ isAllowed, properties, ...props }) => {
    if (isAllowed) {
      return <Route {...props} />
    } else {
      properties.history.push('/login')
      return <Redirect to={{ pathname: '/login' }} push />
    }
  }

export class App extends React.Component {
    componentDidUpdate(prevProps){
        if(this.props.auth !== prevProps.auth){
            if(this.props.auth.token){
                this.props.history.push('/home')
            }
        }
    }

    state = {
        auth: "",
        currentPage: "home"
    }
    render() {
        return (
            <div>
                <Row className="header-bar">
                    <Col xs={24} sm={6} md={6} lg={4} xl={4}>
                        <div className="header-bar__logo"></div>
                    </Col>
                    <Col xs={24} sm={24} md={24} lg={18} xl={18}>
                        <div className={"header-bar__menu" + " " + (this.props.history.location.pathname !== "/login" ? "" : "disabled")}>
                            <Menu
                                onClick={(e) => {this.setState({currentPage:e.key})}}
                                selectedKeys={[this.props.location.pathname.split("/")[1]]}
                                mode="horizontal"
                            >
                                <Menu.Item key="home" onClick={()=>{ this.props.history.push('/home')}}>
                                    <Icon type="home" />Home
                                </Menu.Item>
                                <Menu.Item key="place" onClick={()=>{ this.props.history.push('/place')}}>
                                    <Icon className="icon" type="compass" />Place
                                </Menu.Item>
                                <Menu.Item key="recomended" onClick={()=>{ this.props.history.push('/recomended')}}>
                                    <Icon type="star" />Recomended
                                </Menu.Item>
                                <Menu.Item key="profile" onClick={()=>{ this.props.history.push('/profile')}}>
                                    <Icon type="user" />Profile
                                </Menu.Item>
                            </Menu>
                        </div>
                    </Col>
                    <Col xs={24} sm={4} md={4} lg={2} xl={2}>
                        <div className={"header-bar__logout"  + (this.props.history.location.pathname !== "/login" ? "" : " disabled")}><Icon type="logout" onClick={
                            () => {
                                cookies.remove("auth");
                                this.props.history.push('/login')
                                }
                            }/>
                        </div>
                    </Col>
                </Row>
                <div className="content">
                    <Row className="content__row">
                    <Col className="content__col" span={24}>
                        <Switch>
                        <Route path='/login' component={Login} />
                        <ProtectedRoute
                            isAllowed={cookies.get("auth")}
                            properties={this.props}
                            exact
                            path="/home"
                            component={Home} />
                        <ProtectedRoute
                            isAllowed={cookies.get("auth")}
                            properties={this.props}
                            exact
                            path="/profile"
                            component={Profile} />
                        <ProtectedRoute
                            isAllowed={cookies.get("auth")}
                            properties={this.props}
                            exact
                            path="/place"
                            component={Place} />
                        <ProtectedRoute
                            isAllowed={cookies.get("auth")}
                            properties={this.props}
                            exact
                            path="/recomended"
                            component={Recomended} />
                        <Route path="*" component={() => {
                            this.props.history.push('/home')
                            return <Redirect to={{ pathname: '/home' }} push />
                        }} />
                        </Switch>
                        
                    </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

const withReducer = injectReducer({ key: 'auth', reducer });

const mapStateToProps = createStructuredSelector({
    auth: makeSelectAuth(),
});

const mapDispatchToProps = { login };

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default withRouter(compose(
    withReducer,
    withConnect,
)(App));