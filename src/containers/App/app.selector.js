import { createSelector } from 'reselect';

const selectAuth= (state) => state.get('auth');

const makeSelectAuth = () => createSelector(
  selectAuth,
  (authState) => authState.get('auth')
);

export {
  selectAuth,
  makeSelectAuth
};