export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILED = 'REGISTER_FAILED';

import { loginRequest } from '../../utils/requestHandler';
import { registerRequest } from '../../utils/requestHandler';


export function login(credentials) {
  return (dispatch) => {
    loginRequest(credentials).then(resp => {
        dispatch({
            type: LOGIN_SUCCESS,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: LOGIN_FAILED
      });
    })
  }
}

export function register(credentials) {
  return (dispatch) => {
    registerRequest(credentials).then(resp => {
        dispatch({
            type: REGISTER_SUCCESS,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: REGISTER_FAILED
      });
    })
  }
}