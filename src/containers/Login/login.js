import React from 'react';
import ReactDOM from 'react-dom';

import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import { withRouter } from "react-router";
import { connect } from 'react-redux';
import { compose } from 'redux';

import reducer from '../App/app.reducer';
import { login, register } from './login.actions';
import { makeSelectAuth } from '../App/app.selector';

import {  Form, Input, Icon, Row, Col, Checkbox, Button, Modal} from 'antd';
import './login.styles.scss'

const FormItem = Form.Item;

export class Login extends React.Component {
    state = {
        auth: "",
        confirmDirty: false,
        modalVisible:false
    }

    login = (e) => {
        e.preventDefault();
        this.props.login({
            username:this.props.form.getFieldValue('userName'),
            password:this.props.form.getFieldValue('pass')
        });
      }

    register = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
          if (!err) {
            console.log(values)
            this.props.register(values);
          }
        });
      }

      handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
      }
    
      compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
          callback('Two passwords that you enter is inconsistent!');
        } else {
          callback();
        }
      }
    
      validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
          form.validateFields(['confirm'], { force: true });
        }
        callback();
      }

      showModal = () => {
        this.setState({
          modalVisible:true,
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;

        const formItemLayout = {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 8 },
            },
          };

          const tailFormItemLayout = {
            wrapperCol: {
              xs: {
                span: 24,
                offset: 0,
              },
              sm: {
                span: 24,
                offset: 0,
              },
            },
          };

        return (
            <Row className="login">
                <Col xs={24} sm={24} md={14} lg={14} xl={14} className="login-register__col">
                    <div className="login-register">
                    <div className="login-register__label">Register</div>
                    <Form onSubmit={this.register}>
                        <FormItem
                            {...formItemLayout}
                            label="E-mail"
                            >
                            {getFieldDecorator('email', {
                                rules: [{
                                type: 'email', message: 'The input is not valid E-mail!',
                                }, {
                                required: true, message: 'Please input your E-mail!',
                                }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="Password"
                            >
                            {getFieldDecorator('password', {
                                rules: [{
                                required: true, message: 'Please input your password!',
                                }, {
                                validator: this.validateToNextPassword,
                                }],
                            })(
                                <Input type="password" />
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="Confirm Password"
                            >
                            {getFieldDecorator('confirm', {
                                rules: [{
                                required: true, message: 'Please confirm your password!',
                                }, {
                                validator: this.compareToFirstPassword,
                                }],
                            })(
                                <Input type="password" onBlur={this.handleConfirmBlur} />
                            )}
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label={(
                                <span>
                                Username
                                </span>
                            )}
                            >
                            {getFieldDecorator('username', {
                                rules: [{ required: true, message: 'Please input your username!', whitespace: true }],
                            })(
                                <Input />
                            )}
                        </FormItem>
                        <FormItem {...tailFormItemLayout}>
                            {getFieldDecorator('agreement', {
                                valuePropName: 'checked',
                            })(
                                <Checkbox>I have read the <a onClick={()=>{this.showModal()}}>agreement</a></Checkbox>
                            )}
                        </FormItem>
                        <FormItem {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit">Register</Button>
                        </FormItem>
                    </Form>
                    </div>
                </Col>
                <Col xs={24} sm={24} md={10} lg={10} xl={10} className="login-login__col">
                    <div className="login-login">
                    <div className="login-login__label">Login</div>
                    <Form onSubmit={this.login} className="login-form">
                        <FormItem>
                            {getFieldDecorator('userName', {})(
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                            )}
                        </FormItem>
                        <FormItem>
                            {getFieldDecorator('pass', {})(
                                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                            )}
                        </FormItem>
                        <FormItem>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                Log in
                            </Button>
                        </FormItem>
                    </Form>
                    </div>
                </Col>
                <Modal
                    title="Terms and agreements"
                    visible={this.state.modalVisible}
                    footer={null}
                    className="login-modal"
                    onCancel={() => {this.setState({modalVisible:false})}}
                >
                    <div className="login-modal-content">
                    <h1>1. Updating our Terms</h1>
We work constantly to improve our services and develop new features to make our Products better for you and our community. As a result, we may need to update these Terms from time to time to accurately reflect our services and practices. Unless otherwise required by law, we will notify you (for example, by email or through our Products) at least 30 days before we make changes to these Terms and give you an opportunity to review them before they go into effect. Once any updated Terms are in effect, you will be bound by them if you continue to use our Products.
We hope that you will continue using our Products, but if you do not agree to our updated Terms and no longer want to be a part of the Facebook community, you can delete your account at any time.
<h1>2. Account suspension or termination</h1>
We want Facebook to be a place where people feel welcome and safe to express themselves and share their thoughts and ideas.
If we determine that you have clearly, seriously, or repeatedly violated our terms or policies, including in particular our Community Standards, we may suspend or permanently disable access to your account. We may also suspend or disable your account if we required to do so by law. Where appropriate, we will notify you about your account the next time you try to access it. You can learn more about what you can do if your account has been disabled and how to contact us if you think we have disabled your account by mistake.
If you delete or we disable your account, these Terms shall terminate as an agreement between you and us, but the following provisions will remain in place: 3, 4.2-4.5
<h1>3. Limits on liability</h1>
We will use reasonable skill and care in providing our Products to you and in keeping a safe, secure, and error-free environment, but we cannot guarantee that our Products will always function without disruptions, delays, or imperfections. Provided we have acted with reasonable skill and care, we do not accept responsibility for: losses not caused by our breach of these Terms or otherwise by our acts; losses which are not reasonably foreseeable by you and us at the time of entering into these Terms; any offensive, inappropriate, obscene, unlawful, or otherwise objectionable content posted by others that you may encounter on our Products; and events beyond our reasonable control.
The above does not exclude or limit our liability for death, personal injury, or fraudulent misrepresentation caused by our negligence. It also does not exclude or limit our liability for any other things where the law does not permit us to do so.
<h1>4. Disputes</h1>
We try to provide clear rules so that we can limit or hopefully avoid disputes between you and us. If a dispute does arise, however, it's useful to know up front where it can be resolved and what laws will apply.
If you are a consumer and habitually reside in a Member State of the European Union, the laws of that Member State will apply to any claim, cause of action, or dispute you have against us that arises out of or relates to these Terms or the Facebook Products ("claim"), and you may resolve your claim in any competent court in that Member State that has jurisdiction over the claim. In all other cases, you agree that the claim must be resolved in a competent court in the Republic of Ireland and that Irish law will govern these Terms and any claim, without regard to conflict of law provisions.
<h1>5. Other</h1>
These Terms (formerly known as the Statement of Rights and Responsibilities) make up the entire agreement between you and Facebook Ireland Limited regarding your use of our Products. They supersede any prior agreements.
Some of the Products we offer are also governed by supplemental terms. If you use any of those Products, you will be provided with an opportunity to agree to supplemental terms that will become part of our agreement with you. For instance, if you access or use our Products for commercial or business purposes, such as buying ads, selling products, developing apps, managing a group or Page for your business, or using our measurement services, you must agree to our Commercial Terms. If you post or share content containing music, you must comply with our Music Guidelines. To the extent any supplemental terms conflict with these Terms, the supplemental terms shall govern to the extent of the conflict.
If any portion of these Terms are found to be unenforceable, the remaining portion will remain in full force and effect. If we fail to enforce any of these Terms, it will not be considered a waiver. Any amendment to or waiver of these Terms must be made in writing and signed by us.
You will not transfer any of your rights or obligations under these Terms to anyone else without our consent.
You may designate a person (called a legacy contact) to manage your account if it is memorialized. Only your legacy contact or a person who you have identified in a valid will or similar document expressing clear consent to disclose your content upon death or incapacity will be able to seek disclosure from your account after it is memorialized.
These Terms do not confer any third-party beneficiary rights. All of our rights and obligations under these Terms are freely assignable by us in connection with a merger, acquisition, or sale of assets, or by operation of law or otherwise.
You should know that we may need to change the username for your account in certain circumstances (for example, if someone else claims the username and it appears unrelated to the name you use in everyday life).
We always appreciate your feedback and other suggestions about our products and services. But you should know that we may use them without any restriction or obligation to compensate you, and we are under no obligation to keep them confidential.
We reserve all rights not expressly granted to you.
                    </div>
                </Modal>
            </Row>
        )
    }
}

const withReducer = injectReducer({ key: 'auth', reducer });

const mapStateToProps = createStructuredSelector({
    auth: makeSelectAuth(),
});

const mapDispatchToProps = { login, register };

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default withRouter(compose(
    withReducer,
    withConnect,
    Form.create(),
)(Login));