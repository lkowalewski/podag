export const GET_RECOMENDED_SUCCES = 'GET_RECOMENDED_SUCCES';
export const RECOMENDED_FAILED = 'RECOMENDED_FAILED';

import { sendRequest } from '../../utils/requestHandler';

export function getRecomended(id) {
  console.log("IN RECOMENDED")
  return (dispatch) => {
    sendRequest('GET', '/api/recomended/' + id, {}).then(resp => {
        dispatch({
            type: GET_RECOMENDED_SUCCES,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: RECOMENDED_FAILED
      });
    })
  }
}