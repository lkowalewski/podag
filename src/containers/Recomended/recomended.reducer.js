import { fromJS } from 'immutable';

import {
  GET_RECOMENDED_SUCCES,
  RECOMENDED_FAILED
} from './recomended.actions';

const initialState = fromJS({
  recomended: [],
});

function recomendedReducer(state = initialState, action) {
  switch (action.type) {
    case GET_RECOMENDED_SUCCES: {
      return state.set('recomended', action.payLoad)
    }
    case RECOMENDED_FAILED: {
      return state;
    }
    default:
      return state;
  }
}

export default recomendedReducer;