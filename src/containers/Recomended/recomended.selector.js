import { createSelector } from 'reselect';

const selectRecomended= (state) => state.get('recomended');

const makeSelectRecomended = () => createSelector(
  selectRecomended,
  (recomendedState) => recomendedState.get('recomended')
);

export {
  selectRecomended,
  makeSelectRecomended,
};