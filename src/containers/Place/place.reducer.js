import { fromJS } from 'immutable';

import {
  GET_ALL_PLACES_SUCCES,
  GET_PLACES_SUCCESS,
  ADD_PLACES_SUCCESS,
  UPDATE_PLACE_SUCCESS,
  DELETE_PLACES_SUCCES,
  PLACES_FAILED
} from './place.actions';

const initialState = fromJS({
  places: [],
  allPlacesList: []
});

function placesReducer(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_PLACES_SUCCES: {
      return state.set('allPlacesList', action.payLoad)
    }
    case GET_PLACES_SUCCESS: {
      return state.set('places', action.payLoad.places)
    }
    case ADD_PLACES_SUCCESS: {
      let places = state.get('places');
      places.push(action.payLoad);
      return state.set('places', places)
    }
    case UPDATE_PLACE_SUCCESS: {
      let places = state.get('places');
      console.log(action.payLoad);
      places.push(action.payLoad);
      return state.set('places', places)
    }
    case PLACES_FAILED: {
      return state;
    }
    default:
      return state;
  }
}

export default placesReducer;