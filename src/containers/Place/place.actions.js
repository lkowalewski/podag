export const GET_ALL_PLACES_SUCCES = 'GET_ALL_PLACES_SUCCES';
export const GET_PLACES_SUCCESS = 'GET_PLACES_SUCCESS';
export const ADD_PLACES_SUCCESS = 'ADD_PLACES_SUCCESS';
export const UPDATE_PLACE_SUCCESS = 'UPDATE_PLACE_SUCCESS';
export const DELETE_PLACE_SUCCES = 'DELETE_PLACES_SUCCES';
export const PLACES_FAILED = 'PLACES_FAILED';

import { sendRequest } from '../../utils/requestHandler';

export function getAllPlaces() {
  return (dispatch) => {
    sendRequest('GET', '/api/place/', {}).then(resp => {
        dispatch({
            type: GET_ALL_PLACES_SUCCES,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: PLACES_FAILED
      });
    })
  }
}

export function getPlace(id) {
  return (dispatch) => {
    sendRequest('GET', '/api/users/' + id, {}).then(resp => {
        dispatch({
            type: GET_PLACES_SUCCESS,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: PLACES_FAILED
      });
    })
  }
}

export function addPlace(data) {
  return (dispatch) => {
    sendRequest('POST', '/api/place/', data).then(resp => {
        dispatch({
            type: ADD_PLACES_SUCCESS,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: PLACES_FAILED
      });
    })
  }
}

export function editPlace(id,data) {
  return (dispatch) => {
    sendRequest('PATCH', '/api/place/' + id, data).then(resp => {
        dispatch({
            type: UPDATE_PLACE_SUCCESS,
            payLoad: resp.data
        });
    },(err)=>{
      dispatch({
        type: PLACES_FAILED
      });
    })
  }
}