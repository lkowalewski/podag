import { createSelector } from 'reselect';

const selectPlace= (state) => state.get('places');

const makeSelectPlace = () => createSelector(
  selectPlace,
  (userState) => userState.get('places')
);

const makeSelectListOfPlaces = () => createSelector(
  selectPlace,
  (userState) => userState.get('allPlacesList')
);

export {
  selectPlace,
  makeSelectPlace,
  makeSelectListOfPlaces
};