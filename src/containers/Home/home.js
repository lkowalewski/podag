import React from 'react';
import ReactDOM from 'react-dom';

import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import { withRouter } from "react-router";
import { connect } from 'react-redux';
import { compose } from 'redux';

import { Row, Col} from 'antd';
import './home.styles.scss'

export default class Home extends React.Component {
    render() {
        return (
            <div className="home">
                <Row>
                    <Col className="home-header">   
                        <h2>Welcome to Podag - your pesolinary database of favorite places. Below there is a short introduction to make it easier for you to work with this application.
                         I hope you like the product that I created.</h2>
                    </Col>
                </Row>
                <Row className="home-line">
                    <Col className="home-line-img1" xs={24} sm={24} md={12} lg={12} xl={12}></Col>
                    <Col className="home-line-text" xs={24} sm={24} md={12} lg={12} xl={12}>    
                        Let's start with the basic functionality of the site, i.e. adding and displaying places. 
                        To view our places of entry, enter the "Place" tab. We will see a map, a form and a table. 
                        The site divides the place into the 4th category and assigns the below given colors of markers to them:
                        <ul>
                            <li>Enterteinment - purple</li>
                            <li>Food - green</li>
                            <li>Friends - red</li>
                            <li>Other - grey</li>
                        </ul>                    
                        To add a new place, set a marker on the map and then fill out the forum (boxes marked with stars are required).
                        The application implements a certain functionality to make it easier to add places. When adding a place if in our
                         database we have places nearby, a window with these places pops up. Maybe one of them is the place you're looking for.
                    </Col>
                </Row>
                <Row className="home-line">
                    <Col className="home-line-text" xs={24} sm={24} md={12} lg={12} xl={12}>  
                        The added place can be found in the table. You can quickly move to them by clicking "Go to" in the table on the corresponding record.
                        To edit a place, click on the marker of the place and then on the form, replace the fields you want to edit.
                    </Col>
                    <Col className="home-line-img2" xs={24} sm={24} md={12} lg={12} xl={12}> </Col>   
                </Row>
                <Row className="home-line">
                    <Col className="home-line-img3" xs={24} sm={24} md={12} lg={12} xl={12}></Col>
                    <Col className="home-line-text" xs={24} sm={24} md={12} lg={12} xl={12}>
                        To see the places recommended for you, go to the "Recomended" tab. There, we will see a table with places recommended on the principle 
                        of "places highly rated by users who highly appreciate the places we like." If you click on the "Show" option in the table, a window will
                         appear with the location of the given place.
                    </Col>
                </Row>
                <Row className="home-line">
                    <Col className="home-line-text" xs={24} sm={24} md={12} lg={12} xl={12}>
                        In the profiles tab, we can edit our personal details and account attributes.
                    </Col>
                    <Col className="home-line-img4" xs={24} sm={24} md={12} lg={12} xl={12}></Col>   
                </Row>
            </div>
        )
    }
}
