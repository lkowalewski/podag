import express from 'express';
import User from '../models/userModel'
import Place from '../models/placeModel';
import jwt from 'jsonwebtoken';
import bcryptjs from 'bcryptjs';

const usersRouter = express.Router();

usersRouter.route('/')
.post((req, res) => {
    let user = new User({
        username:req.body.username,
        password:req.body.password,
        mail:req.body.mail,
    });
    bcryptjs.hash(req.body.password, 10, function(err, hash) {
        user.password = hash;
        user.save(function (err) {
            if(!err){
                res.status(201).send(req.body) 
            }
            else res.status(403).send({ 
                success: false, 
                message: 'Error while adding user.' 
            });
        });

    });     
})

usersRouter.use('/',(req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    const app = req.app
    if (token) {
      jwt.verify(token, app.get('superSecret'), function(err, decoded) {       
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });       
        } else {
          req.decoded = decoded;         
          next();
        }
      }); 
    } else {
      return res.status(403).send({ 
          success: false, 
          message: 'No token provided.' 
      });
    }
  });


usersRouter.route('/')
    .get((req, res) => {
        User.find({}, (err, users) => {
            res.json(users)
        })
    })

usersRouter.use('/:userId', (req, res, next)=>{
    User.findById( req.params.userId, (err,user)=>{
        if(err)
            res.status(500).send(err)
        else {
            req.user = user;
            next()
        }
    }).populate('places')
    
})
    

usersRouter.route('/:userId')
    .get((req, res) => {
        res.json(req.user)
    })
    .put((req,res) => {
        req.user.name = req.body.name;
        req.user.surname = req.body.surname;
        req.user.save()
        res.json(req.user)
    })
    .patch((req,res)=>{
        if(req.body._id){
            delete req.body._id;
        }
        for( let b in req.body ){
            req.user[b] = req.body[b];
        }
        req.user.save();
        res.json(req.user);
    })
    .delete((req,res)=>{
        req.user.remove(err => {
            if(err){
                res.status(500).send(err)
            }
            else{
                res.status(204).send('removed')
            }
        })
    })

export default usersRouter;