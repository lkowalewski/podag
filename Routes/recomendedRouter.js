import express from 'express';
import Place from '../models/placeModel';
import User from '../models/userModel';
import jwt from 'jsonwebtoken';
import _ from 'underscore'

const recomendedRouter = express.Router();

recomendedRouter.use('/',(req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    const app = req.app
    if (token) {
      jwt.verify(token, app.get('superSecret'), function(err, decoded) {       
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });       
        } else {
          req.decoded = decoded;         
          next();
        }
      }); 
    } else {
      return res.status(403).send({ 
          success: false, 
          message: 'No token provided.' 
      });
    }
  });

recomendedRouter.use('/:userId', (req, res, next)=>{
    User.findById( req.params.userId, (err,user)=>{
        if(err)
            res.status(500).send(err)
        else {
            req.userInfo = JSON.parse(JSON.stringify(user));
            next()
        }
    })
})

recomendedRouter.route('/:userId')
    .get((req, res) => {
        let usersList;
        let recomended=[];
        Place.find({}, (err, places) => {
            let placesList = JSON.parse(JSON.stringify(places));
            usersList = getUsersLikeMe(req.userInfo, getUserRecomended(req.userInfo,placesList));
            User.find({}, (err, users) => {
                let AllUsersList = JSON.parse(JSON.stringify(users));
                for(let userId in usersList){
                    for(let idx in AllUsersList){
                        if(AllUsersList[idx].username === usersList[userId]){
                            let recomendedPlaces = getUserRecomended(AllUsersList[idx], placesList);
                            recomended = recomended.concat(recomendedPlaces);
                        }
                    }
                }
                res.status(200).send(cleanData(recomended, req.userInfo.places));
            })
        }) 
    })

export default recomendedRouter;

function getUserRecomended(userInfo,placesList){
    let recomendedByUser = [];
    for(let idx in userInfo.places){ //dla kazdego miejsca uzytkownika
        for(let placeId in placesList){ // dla kazdego miejsca
            let rating;
            let selectedUserPlaceObject = _.findWhere(placesList[placeId].users,{name : userInfo.username}); // obiekt miejsca odpowiedzialny za ocene danego uzytkownika
            selectedUserPlaceObject ? rating = selectedUserPlaceObject.rating : rating = 0;
            if(placesList[placeId]._id.toString() == userInfo.places[idx].toString() && rating > 3){
                recomendedByUser.push(placesList[placeId])
            }
        }
    }
    return recomendedByUser;
}

function getUsersLikeMe(userInfo, recomendedPlaces){
    let usersLikeMe = [];
    for(let placeId in recomendedPlaces){
        for(let userId in recomendedPlaces[placeId].users){ // po innych uzytkownikach tego miejsca
            if(recomendedPlaces[placeId].users[userId].name !== userInfo.username){
                usersLikeMe.push(recomendedPlaces[placeId].users[userId].name );
            }
        }
    }
    return usersLikeMe;
}

function cleanData(arrayToClean, cleaningData){
    let newArray = [];
    for(let idx in cleaningData){
        newArray = arrayToClean.filter( el => el._id !== cleaningData[idx] ); 
    }
    let unique = [...new Set(newArray)];
    return unique
}