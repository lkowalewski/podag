import express from 'express';
import Place from '../models/placeModel';
import User from '../models/userModel';
import jwt from 'jsonwebtoken';

const placesRouter = express.Router();

placesRouter.use('/',(req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    const app = req.app
    if (token) {
      jwt.verify(token, app.get('superSecret'), function(err, decoded) {       
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });       
        } else {
          req.decoded = decoded;         
          next();
        }
      }); 
    } else {
      return res.status(403).send({ 
          success: false, 
          message: 'No token provided.' 
      });
    }
  });


placesRouter.route('/')
    .get((req, res) => {
        Place.find({}, (err, place) => {
            res.json(place)
        })
    })
    .post((req, res) => {
        let place = new Place({
            name:req.body.name,
            lat:req.body.lat,
            lng:req.body.lng,
            address:req.body.address,
            description:req.body.description,
            group:req.body.group,
            users:[{
                name:req.body.username,
                rating:req.body.rate
            }]
        });      
        place.save();
        
        User.findById( req.body.userId, (err,user)=>{
            user.places.push(place._id)
            user.save();
        })
        res.status(201).send(place) 
    })

placesRouter.use('/:placeId', (req, res, next)=>{
    Place.findById( req.params.placeId, (err,place)=>{
        if(err)
            res.status(500).send(err)
        else {
            req.place = place;
            next()
        }
    })
})
    

placesRouter.route('/:placeId')
    .get((req, res) => {
        res.json(req.place)
    })
    .patch((req,res)=>{
        if(req.body._id){
            delete req.body._id;
        }
        for( let b in req.body ){
            req.place[b] = req.body[b];
        }
        req.place.save();
        res.status(200).send(req.place);
    })
    .delete((req,res)=>{
        req.place.remove(err => {
            if(err){
                res.status(500).send(err)
            }
            else{
                res.status(204).send('removed')
            }
        })
    })

export default placesRouter;