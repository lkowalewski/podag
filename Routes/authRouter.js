import express from 'express';
import jwt from 'jsonwebtoken';
import User from '../models/userModel'
import bcryptjs from "bcryptjs"

const authRouter = express.Router();

authRouter.post('/', function (req, res) {
    User.findOne({
        username: req.body.username
    }, function (err, user) {
        if (err) throw err;
        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {
            bcryptjs.compare(req.body.password, user.password, function(err, response) {
                if(response) {
                    const payload = {
                        user: true
                    };
                    const app = req.app;
                    const token = jwt.sign(payload, app.get('superSecret'), {
                        expiresIn: '24h' // expires in 24 hours
                    });
                    res.status(201).send({
                        success: true,
                        message: 'Enjoy your token!',
                        token: token,
                        username:user.username,
                        id:user.id
                    });
                } else {
                    res.status(403).send({ success: false, message: 'Authentication failed. Wrong password.' });
                } 
              });
        }
    });
});

authRouter.use('/',(req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    if (token) {
      jwt.verify(token, app.get('superSecret'), function(err, decoded) {       
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });       
        } else {
          req.decoded = decoded;         
          next();
        }
      }); 
    } else {
      return res.status(403).send({ 
          success: false, 
          message: 'No token provided.' 
      });
    }
  });

  export default authRouter;