import express from 'express';

import usersRouter from './Routes/usersRouter';
import placesRouter from './Routes/placesRouter';
import authRouter from './Routes/authRouter';
import recomendedRouter from './Routes/recomendedRouter'

import mongoose from 'mongoose';
import bodyParser from 'body-parser'
import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackConfig from './webpack.config.js';


const db = mongoose.connect('mongodb://admin:admin@localhost:27017/podag');
const app = express();
const port = process.env.PORT || 8081;
app.set('superSecret', 'podag');

app.use(webpackMiddleware(webpack(webpackConfig)));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// routes go here
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);
app.use('/api/place', placesRouter);
app.use('/api/recomended', recomendedRouter);
app.use(function(req, res) {
    res.redirect('/');
});
app.listen(port, () => {
    console.log(`http://localhost:${port}`)
})