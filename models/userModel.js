import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const userModel = new Schema({
    username: { 
        type: String, 
        unique:true 
    },
    password: { 
        type: String 
    },
    mail: { 
        type: String, 
        unique:true 
    },
    name: { 
        type: String 
    },
    surname: { 
        type: String 
    },
    city: { 
        type: String 
    },
    age: { 
        type: String 
    },
    places:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'place'
    }]
})
export default mongoose.model('user', userModel)