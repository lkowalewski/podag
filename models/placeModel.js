import mongoose from 'mongoose';
const Schema = mongoose.Schema;
const placeModel = new Schema({
    name: { 
        type: String,
        unique:true  
    },
    lng: { 
        type: String 
    },
    lat: { 
        type: String 
    },
    address: { 
        type: String 
    },
    group:{
        type: String
    },
    description:{
        type: String
    },
    users:[{
        _id : false,
        name:{
            type:String
        },
        rating:{
            type:String
        }
    }]
})
export default mongoose.model('place', placeModel)